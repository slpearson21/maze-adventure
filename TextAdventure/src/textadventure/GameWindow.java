/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package textadventure;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 *
 * @author kreature
 */
public class GameWindow extends JFrame {
    
    JPanel gameWindow;
    JTextArea outputText;
    JButton up;
    JButton down;
    JButton left;
    JButton right;
    JButton action;
    
    public GameWindow() {
        gameWindow = new JPanel(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        add(gameWindow);
    }
    
}
